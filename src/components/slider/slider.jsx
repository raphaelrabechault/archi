// React & Hooks Import
import React, { useState, useEffect } from 'react';
// CSS & Icon Imports
import './slider.css';
import Down_Arrow from '../../assets/Icons/Down_Arrow.png'
import Slider_Arrow from '../../assets/Icons/Slider_Arrow.png'
import { FaTimes } from "react-icons/fa";

// Image Slider Component
const ImageSlider = ({ images, description, language, sliderKey }) => {

  // Handle State of Current Index and Description Box
  const [currentIndex, setCurrentIndex] = useState(0);
  const [viewDesc, setViewDesc] = useState(false);

  // Handle View Description Hover Effect
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  // Left and Right Arrow Key Handle
  useEffect(() => {
    const handleKeyDown = (event) => {
      if (event.keyCode === 37) {
        goToPrevSlide();
      } else if (event.keyCode === 39) {
        goToNextSlide();
      }
    };
    window.addEventListener('keydown', handleKeyDown);
    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [currentIndex, sliderKey]);

  // To Handle Touch Swipe of Slider during Mobile View
  const [startIndex, setStartIndex] = useState(0);

  const handleTouchStart = (e) => {
    setStartIndex(e.touches[0].clientX);
  };

  const handleTouchMove = (e) => {
    const currentIndex = e.touches[0].clientX;
    const difference = startIndex - currentIndex;

    if (difference > 50) {
      goToNextSlide();
    } else if (difference < -50) {
      goToPrevSlide();
    }
  };

  // Prev. and Next Sliding Feature
  const getNextIndex = () => {
    return (currentIndex + 1) % images.length;
  };

  const getPrevIndex = () => {
    return (currentIndex - 1 + images.length) % images.length;
  };

  const goToPrevSlide = () => {
    const index = (currentIndex - 1 + images.length) % images.length;
    setCurrentIndex(index);
  };

  const goToNextSlide = () => {
    const index = (currentIndex + 1) % images.length;
    setCurrentIndex(index);
  };

  // Handle Thumbnail Direct Selection
  const goToSlide = (index) => {
    setCurrentIndex(index);
  };

  //Description Word - EN, FR and SPA
  const descWord = {
    EN: "View Description",
    FR: "Vue de la description",
    SPA: "Ver descripción"
  }

  return (
    <div className="carousel-container">
      <div className="carousel-slider" onTouchStart={handleTouchStart} onTouchMove={handleTouchMove}>
        <img src={Slider_Arrow} className='leftArrow' alt="Left Arrow Icon" onClick={goToPrevSlide} preload />
        <div className="slidePrev" style={{ backgroundImage: `url(${images[getPrevIndex()].image})` }}>
        </div>
        <div className={`slide ${viewDesc ? 'focused' : ''}`} style={{ backgroundImage: `url(${images[currentIndex].image})` }}>
          {viewDesc ? (<div className='white-overlay'>
            <FaTimes className='cancel-btn' onClick={() => setViewDesc(false)} />
            <p>{description[language]}</p>
          </div>) : (<div className='no-overlay'>
            <div onClick={() => setViewDesc(true)} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
              <img className='up-arrow' src={Down_Arrow} alt="Up Arrow"  preload />
              {isHovered && (
                <p>{descWord[language]}</p>
              )}
            </div>
          </div>)}
        </div>
        <div className="slideNext" style={{ backgroundImage: `url(${images[getNextIndex()].image})` }}>
        </div>
        <img src={Slider_Arrow} className='rightArrow' alt="Right Arrow Icon" onClick={goToNextSlide} preload />
      </div>
      <p className='image-title'>{images[currentIndex][language]}</p>
      <div className="thumbnails">
        <div className='thumbnail-heading'>
          {language === 'SPA' ? (<p>ARQUITECTURA</p>) : (<p>ARCHITECTURE</p>)}
        </div>
        <div className='thumbnails-container'>
          <div className='thumbnail-images'>
            {images.map((image, index) => (
              <img
                key={index}
                src={image.image}
                alt={images[currentIndex][language]}
                className={index === currentIndex ? 'active' : ''}
                onClick={() => goToSlide(index)}
                preload
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ImageSlider;
